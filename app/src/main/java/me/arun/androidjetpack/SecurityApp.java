package me.arun.androidjetpack;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;

import me.arun.androidjetpack.Pagination.localDb.AppDataBase;
import me.arun.androidjetpack.Pagination.model.Player;
import me.arun.androidjetpack.Pagination.model.Players;
import me.arun.androidjetpack.util.LocalData;



public class SecurityApp extends Application {


    public LocalData localData;
    public AppDataBase appDataBase;
    String TAG="SecurityApp";
    public AppDataBase getAppDataBase()
    {
        return appDataBase;
    }

    public void setAppDataBase(AppDataBase appDataBase)
    {
        this.appDataBase = appDataBase;
    }


    public void onCreate() {
        super.onCreate();
        localData = new LocalData(getApplicationContext());
        appDataBase=AppDataBase.getAppDatabase(getApplicationContext());
        localData.setBooleanPreferenceValue("isFingerPrintNeed", true);



        if (!localData.getBooleanPreferenceValue(getString(R.string.isDataStored)))
        {
            String json=loadJSONFromAsset();
            Log.d(TAG, "onCreate: "+json);
            Gson gson=new GsonBuilder().create();
            if (json!=null)
            {
                Players modelBook=gson.fromJson(json,Players.class);
                if (modelBook!=null)
                {
                    Player[] books= modelBook.getPlayers().toArray(new Player[modelBook.getPlayers().size()]);
                    Log.d(TAG, "onCreate: "+books.length);
                    appDataBase.bookDao().insert(books);
                    localData.setBooleanPreferenceValue(getString(R.string.isDataStored),true);
                }
            }
        }
    }


    public String loadJSONFromAsset()
    {
        String json = null;
        try {
            InputStream is = getAssets().open("DBData.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * A method to get the decide to the fingerprint has to used or not
     * @return it return the true if the fingerprint sensor need to authenticate
     */
    public boolean getIsFingerPrintNeed()
    {
       return localData.getBooleanPreferenceValue("isFingerPrintNeed");
    }

}
