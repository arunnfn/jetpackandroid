package me.arun.androidjetpack.navigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.navigation.Navigation;
import me.arun.androidjetpack.R;

import static android.support.constraint.Constraints.TAG;


public class PlaceListFragment extends Fragment {
    Button btPlaceList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_place_list, container, false);
        btPlaceList=(Button)view.findViewById(R.id.btPlaceDetail);
        Bundle bundle=getArguments();
        Log.d(TAG, "onCreateView: "+bundle.get("lowStepNumber"));
        btPlaceList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.actionPlaceListToDetail);
            }
        });
        return view;

    }

}

