package me.arun.androidjetpack.navigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.navigation.Navigation;
import me.arun.androidjetpack.R;

import static android.support.constraint.Constraints.TAG;

public class HomeFragment extends Fragment {

    Button btPlaceList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle bundle=new Bundle();
        bundle.putInt("lowStepNumber",4);
        View view= inflater.inflate(R.layout.fragment_home, container, false);
        btPlaceList=(Button)view.findViewById(R.id.btPlaceList);
        btPlaceList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Navigation.findNavController(view).navigate(R.id.actionHomeToPlaceList);
                Navigation.findNavController(view).navigate(R.id.actionHomeToPlaceList,bundle);
            }
        });
        return view;

    }
}
