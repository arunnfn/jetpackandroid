package me.arun.androidjetpack.navigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.navigation.Navigation;
import me.arun.androidjetpack.R;

public class PlaceDetailFragment extends Fragment
{

    Button btPlaceList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_place_detail, container, false);
        btPlaceList=(Button)view.findViewById(R.id.btPlaceDetail);
        btPlaceList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.actionPlaceDetailToDetailActivity);
            }
        });
        return view;

    }
}
