package me.arun.androidjetpack.Pagination;

import me.arun.androidjetpack.Pagination.model.StackApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * A interface for the api call
 */
public interface Api {

    /**
     * a abstract method for the get list form stack overflow
     * @param page page no
     * @param pagesize no of items for the page
     * @param site  sitename
     * @return it returns the reponse of the api call
     */
    @GET("answers")
    Call<StackApiResponse> getAnswers(@Query("page") int page, @Query("pagesize") int pagesize, @Query("site") String site);
}
