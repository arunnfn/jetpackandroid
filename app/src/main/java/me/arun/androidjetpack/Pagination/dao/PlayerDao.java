package me.arun.androidjetpack.Pagination.dao;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import me.arun.androidjetpack.Pagination.model.Player;


/**
 * A data access Class for the Player Table
 */
@Dao
public interface PlayerDao
{
    /**
     * A method to insert the records in player table
     * @param players a param has the array of the Player object or Single object
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Player... players);

    /**
     * A method to get the select the player using his id
     * @param id a value of the id of the player
     * @return it returns the player object of the given id
     */
    @Query("Select * FROM player WHERE pid== :id")
    Player select(String id);

    /**
     * A method for the get the list of data using the below Query in the form of the Factory data model
     * @return it returns the Data source data model for the list of players
     */
    @Query("SELECT * FROM player ORDER BY pid ASC")
    DataSource.Factory<Integer,Player> liveListData();


}
