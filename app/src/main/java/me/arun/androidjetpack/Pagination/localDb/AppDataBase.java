package me.arun.androidjetpack.Pagination.localDb;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import me.arun.androidjetpack.Pagination.dao.PlayerDao;
import me.arun.androidjetpack.Pagination.model.Player;


/**
 * A class for the initialize the AppDataBase and abstract method for the dao
 */
@Database(entities = {Player.class}, version = 1, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase
{
    private static AppDataBase instance;
    public abstract PlayerDao bookDao();

    public static AppDataBase getAppDatabase(Context context)
    {
        if (instance == null)
        {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDataBase.class,
                    "playerDb")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    //        .addMigrations(MIGRATION_1_2,MIGRATION_2_3)
}