package me.arun.androidjetpack.Pagination.model;

import java.util.List;


/**
 * Model class for the Api Response
 */

public class StackApiResponse {
    public List<Item> items;
    public boolean has_more;
    public int quota_max;
    public int quota_remaining;
}
