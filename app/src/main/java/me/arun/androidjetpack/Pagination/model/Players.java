
package me.arun.androidjetpack.Pagination.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Players {

    @SerializedName("players")
    private List<Player> mPlayers;

    public List<Player> getPlayers() {
        return mPlayers;
    }

    public void setPlayers(List<Player> players) {
        mPlayers = players;
    }

}
