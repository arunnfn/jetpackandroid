package me.arun.androidjetpack.Pagination;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import me.arun.androidjetpack.Pagination.dao.PlayerDao;
import me.arun.androidjetpack.Pagination.model.Item;
import me.arun.androidjetpack.Pagination.model.Player;
import me.arun.androidjetpack.SecurityApp;


/**
 * A class created for the ViewModel but for ViewModel class, class should extend the ViewModel class or AndroidViewModel
 */
public class ItemViewModel extends AndroidViewModel {

    LiveData<PagedList<Item>> itemPagedList;

   //for db
    private static final int INITIAL_LOAD_KEY = 0;
    private static final int PAGE_SIZE = 10;
    private static final int PREFETCH_DISTANCE = 5;


    public SecurityApp pagingLibraryApplication;
    public LivePagedListBuilder livePlyerList;
    public final LiveData<PagedList<Player>> playerList;

    public ItemViewModel(@NonNull Application application)
    {
        super(application);
        ItemDataSourceFactory itemDataSourceFactory = new ItemDataSourceFactory();
//        liveDataSource = itemDataSourceFactory.getItemLiveDataSource();


        // a  configuration for the pagination
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(ItemDataSource.PAGE_SIZE).build();

        itemPagedList = new LivePagedListBuilder(itemDataSourceFactory, pagedListConfig)
                .build();


        pagingLibraryApplication =(SecurityApp) application;
        PlayerDao playerDao= pagingLibraryApplication.appDataBase.bookDao();
        playerList = new LivePagedListBuilder<>(
                playerDao.liveListData(), /* page size */ PAGE_SIZE).setInitialLoadKey(INITIAL_LOAD_KEY).build();
//        livePlyerList=new LivePagedListBuilder(playerDao.liveListData(),new PagedList.Config.Builder().setPageSize(PAGE_SIZE).setPrefetchDistance(PREFETCH_DISTANCE).setPageSize(PAGE_SIZE).build());

    }
}
