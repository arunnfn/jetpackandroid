package me.arun.androidjetpack.Pagination;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import me.arun.androidjetpack.Pagination.adapter.PlayerAdapter;
import me.arun.androidjetpack.R;


public class MainActivity extends AppCompatActivity
{
    String TAG = "MainActivity";
    RecyclerView recyclerView;
    ItemViewModel itemViewModel;
    PlayerAdapter playerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Log.d(TAG, "onCreate: "+ DebugDB.getAddressLog());
        recyclerView=(RecyclerView)findViewById(R.id.rvPlayers);
        playerAdapter=new PlayerAdapter(this);
        // a Viewmodel object initialization
        itemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);

        // a observer for the player list in android
        itemViewModel.playerList.observe(this,pagedList->playerAdapter.submitList(pagedList));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(playerAdapter);
    }
}
